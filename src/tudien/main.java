package tudien;

import java.util.Scanner;

public class main {
public static void main(String[] args) {
	tuDien td = new tuDien();
	int luaChon = 0;
	Scanner sc = new Scanner(System.in);
	do {
		System.out.println("-------------------------MENU-------------------------");
		System.out.println("1. Thêm từ");
		System.out.println("2. Xóa từ");
		System.out.println("3. Tra từ");
		System.out.println("4. In ra danh sách các từ");
		System.out.println("5. Lấy số lượng từ");
		System.out.println("6. Xóa tất cả các từ khóa");
		System.out.println("0. Thoát khỏi chương trình");
		System.out.println("Nhập lựa chọn: "); 
		luaChon = sc.nextInt();
		sc.nextLine();
		if(luaChon == 1) {
			System.out.println("Nhập từ cần thêm"); String key = sc.nextLine();
			System.out.println("Nhập ý nghĩa"); String data = sc.nextLine();
			td.add(key, data);
		} else if(luaChon == 2) {
			System.out.println("Nhập từ muốn xoá"); String key = sc.nextLine();	
			td.delete(key);
		} else if(luaChon == 3){
			System.out.println("Nhập từ cần tra"); String key = sc.nextLine();	
			System.out.println("Từ cần tra có nghĩa là :" +td.traTu(key));		
		} else if(luaChon == 4){
			td.in();
		} else if(luaChon == 5){
			System.out.println("Từ điển có: " +td.soLuong() + " từ");
		} else {
			td.xoaAll();;
		}
	} while (luaChon !=0);
  }
}

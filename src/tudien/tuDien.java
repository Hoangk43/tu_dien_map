package tudien;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class tuDien {
	private Map<String, String> data = new TreeMap<String, String>();

//	public tuDien() {
//	}
	
	public String add(String key, String value) {
		return data.put(key, value);
	}
	
	public String delete(String key) {
		return data.remove(key);
	}
	
	public String traTu(String key) {
		return data.get(key);
	}
	
	public void in() {
		for (String key : data.keySet()) {
			System.out.println(key + " " + data.get(key));
		}	
//		Set tapHopTuKhoa = data.keySet();
//		System.out.println(Arrays.toString(tapHopTuKhoa.toArray()));
	}
	
	public int soLuong() {
		return data.size();
	}
	
	public void xoaAll() {
		data.clear();
	}
}
